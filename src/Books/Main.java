package Books;

import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


public class Main {


    static Scanner input =new Scanner (System.in);
    private static String categoryForsearch;
    private static int priceToCompare;


    public static void main(String[] args) {
        RealBook o1984 = new RealBook("1984", 55, 4.19, "Hardcover", 0.9);
        RealBook LOTR = new RealBook("Lord of the Rings", 66, 4.37, "Hardcover", 1);
        RealBook fightClub = new RealBook("Fight Club", 44, 4.16, "Softcover", 0.77);
        ElectlonicBook postOffice = new ElectlonicBook("Post office", 33, 3.97, "EPUB", 1);
        ElectlonicBook fahrenheit451 = new ElectlonicBook("Fahrenheit 451", 43, 3.99, "PDF", 0.95);
        Category fiction = new Category("fiction");
        Category non_fiction = new Category("non_fiction");
        Category drama = new Category("drama");
        Category fantasy = new Category("fantasy");

        HashMap<String, String> bookByCategory = new HashMap<String, String>();
        bookByCategory.put(o1984.getName(), fiction.getCategoryName());
        bookByCategory.put(LOTR.getName(), fantasy.getCategoryName());
        bookByCategory.put(fightClub.getName(), non_fiction.getCategoryName());
        bookByCategory.put(postOffice.getName(), drama.getCategoryName());
        bookByCategory.put(fahrenheit451.getName(), fantasy.getCategoryName());

        ArrayList<Book> listOfBooks = new ArrayList<Book>();
        listOfBooks.add(o1984);
        listOfBooks.add(LOTR);
        listOfBooks.add(fightClub);
        listOfBooks.add(postOffice);
        listOfBooks.add(fahrenheit451);

        System.out.println("Enter category:");
        categoryForsearch = input.nextLine();
        searchBookByCategory(bookByCategory, categoryForsearch);

        System.out.println("Enter price for filtering:");
        priceToCompare = input.nextInt();
        searchBookbyPrice(listOfBooks, priceToCompare);


        //My methods
        searchBookOnSale(listOfBooks);
        searchAllBooks(listOfBooks);


    }


    public static void searchBookByCategory(HashMap<String, String> bookByCategory, String categoryForsearch) {
        for (String category : bookByCategory.keySet()) {
            if (bookByCategory.get(category).equals(categoryForsearch)) {
                System.out.printf("Book with %s category: %s\n", categoryForsearch, category);
            }
        }
        System.out.println();
    }

    public static void searchBookOnSale(ArrayList<Book> listOfBooks){
        for(int i = 0; i<listOfBooks.size(); i++){
            boolean isOnSale = listOfBooks.get(i).checkSale();
            if(isOnSale == true){
            System.out.printf("%s book is on sale, the prise is: \n", listOfBooks.get(i).getName());
            System.out.println(listOfBooks.get(i).calculatePriceWithOnSave(listOfBooks.get(i).getPrice()));
            }
        }
        System.out.println();
    }

    //lambda
    public static void searchAllBooks(ArrayList<Book> listOfBooks){
        System.out.println("List of all books:");
        listOfBooks.forEach(name -> System.out.println(name.getName()));
        System.out.println();
    }

    public static void searchBookbyPrice(ArrayList<Book> listOfBooks, int priceToCompare){
        System.out.println("Books with price lower then 50:");
        for(int i = 0; i<listOfBooks.size(); i++){
            if(listOfBooks.get(i).getPrice()<priceToCompare){
                System.out.println(listOfBooks.get(i).getName());
            }

        }
        System.out.println();

    }
}