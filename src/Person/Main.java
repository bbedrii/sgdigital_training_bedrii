package Person;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static Scanner input = new Scanner(System.in);
    static String subjectToSearch;
    static String nameToSearch;
    static String facultyToSearch;


    public static void main(String[] args) {
        Teacher Bohdan = new Teacher("Bohdan", 32, "Law", 50);
        Teacher Sasha = new Teacher("Sasha", 18, "Management", 60);
        Teacher Maria = new Teacher("Maria", 19, "Technologies", 60);
        Student Nastia = new Student("Nastia", 20, "FancyStudies", 99);

        ArrayList<Teacher> teachersList = new ArrayList<Teacher>();
        teachersList.add(Bohdan);
        teachersList.add(Sasha);
        teachersList.add(Maria);

        ArrayList<Student> studentList = new ArrayList<>();
        studentList.add(Nastia);

        ArrayList<Person> personList = new ArrayList<Person>();
        personList.addAll(teachersList);
        personList.addAll(studentList);

        //my methods
        System.out.println("Enter subject to filter teachers:");
        subjectToSearch = input.nextLine();
        searchTeacherBySubject(teachersList);

        System.out.println("Enter the name for filter persons:");
        nameToSearch = input.nextLine();
        searchPersonByName(personList);

        System.out.println("Enter faculty to filter students:");
        facultyToSearch = input.nextLine();
        searchStudentByFaculty(studentList);


        System.out.println(Bohdan.calculateSalary(Bohdan.getDayRate(), 22));
        System.out.println(Sasha.calculateSalary(Sasha.getDayRate(), 20, 5));

    }

    public static void searchTeacherBySubject(ArrayList<Teacher> teachersList) {
        for (int i = 0; i < teachersList.size(); i++) {
            String theGuy = teachersList.get(i).getName();
            if (subjectToSearch.equals(teachersList.get(i).getSubject())) {
                System.out.println(theGuy + " " + teachersList.get(i).isAttended());
            }
        }
        System.out.println();
    }

    public static void searchPersonByName(ArrayList<Person> personList) {
        for (int i = 0; i < personList.size(); i++) {
            String theGuy = personList.get(i).getName();
            if (nameToSearch.equals(theGuy)) {
                System.out.println(theGuy + " age: " + personList.get(i).getAge());
            }
        }
        System.out.println();
    }

    public static void searchStudentByFaculty(ArrayList<Student> studentList) {
        for (int i = 0; i < studentList.size(); i++) {
            String theGuy = studentList.get(i).getName();
            if (facultyToSearch.equals(studentList.get(i).getFaculty())) {
                System.out.printf("%s That's the one! \n%s \n", theGuy, studentList.get(i).isAttended());
            }
        }
        System.out.println();
    }
}
